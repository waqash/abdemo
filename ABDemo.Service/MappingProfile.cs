﻿using ABDemo.DataAccess.Model;
using ABDemo.DataAccess.Repository;
using ABDemo.Dto;
using ABDemo.Dto.Sales;
using ABDemo.Service.Resolvers;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ABDemo.Service
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<Item, SaleItemDto>()
                //.ForMember(dest => dest.Category, src => src.MapFrom<CategoryResolver>())
                .ForMember(dest => dest.Category, src => src.MapFrom(it => it.Category.CategoryName))
                .ForMember(dest => dest.SubCategory, src => src.MapFrom(it => it.SubCategory.SubCategoryName))
                .ForMember(dest => dest.Name, src => src.MapFrom(it => it.ItemName))
                .ForMember(dest => dest.Name, src => src.Ignore())
                .ForMember(dest => dest.Tags, src => src.MapFrom(it => it.ItemTag));

            CreateMap<ItemTag, ItemTagDto>()
                .ForMember(dest => dest.Id, src => src.MapFrom(it => it.TagId))
                .ForMember(dest => dest.tagName, src => src.MapFrom(it => Sales.SalesService.GetTagNameFromTag(it.Tag)))
                .ReverseMap();

            CreateMap<Demo1, Demo2Dto>();
        }
    }
}