﻿using ABDemo.DataAccess.Model;
using ABDemo.DataAccess.Repository;
using ABDemo.Dto;
using ABDemo.Dto.Sales;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ABDemo.Service.Sales
{
    public interface ISalesService
    {
        Task<List<SaleItemDto>> GetItemsList();
        Demo2Dto GetDemo();
    }
    public class SalesService : ISalesService
    {
        private IItemsRepository _itemsRepository;
        private IMapper _mapper;
        public SalesService(IItemsRepository itemsRepository, IMapper mapper)
        {
            _itemsRepository = itemsRepository;
            _mapper = mapper;
        }

        public async Task<List<SaleItemDto>> GetItemsList()
        {
            List<Item> itemsList = await _itemsRepository.GetItemsList();
            List<SaleItemDto> result =  _mapper.Map<List<SaleItemDto>>(itemsList);
            return result;
        }

        public static string GetTagNameFromTag(Tag tag)
        {
            return tag.TagName;
        }

        public Demo2Dto GetDemo()
        {
            Demo1 demo = _itemsRepository.GetDemo1();
            return _mapper.Map<Demo2Dto>(demo);
        }
    }
}
