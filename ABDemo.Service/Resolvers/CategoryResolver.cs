﻿using ABDemo.DataAccess.Model;
using ABDemo.DataAccess.Repository;
using ABDemo.Dto.Sales;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;

namespace ABDemo.Service.Resolvers
{
    public class CategoryResolver : IValueResolver<Item, SaleItemDto, string>
    {
        private IItemsRepository _itemsRepository;
        public CategoryResolver(IItemsRepository itemsRepository)
        {
            _itemsRepository = itemsRepository;
        }
        public string Resolve(Item source, SaleItemDto destination, string destMember, ResolutionContext context)
        {
            return _itemsRepository.GetCatgoryNameById(source.CategoryId);
        }
    }
}
