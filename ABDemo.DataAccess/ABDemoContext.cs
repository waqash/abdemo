﻿using ABDemo.DataAccess.Model;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace ABDemo.DataAccess
{
    public class ABDemoContext : DbContext
    {
        public ABDemoContext()
        {

        }
        public ABDemoContext(DbContextOptions<ABDemoContext> options) : base(options) { }

        public virtual DbSet<Item> Item { get; set; }
        public virtual DbSet<Category> Category { get; set; }
        public virtual DbSet<SubCategory> SubCategory { get; set; }
        public virtual DbSet<Tag> Tag { get; set; }
        public virtual DbSet<ItemTag> ItemTag { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<ItemTag>().HasKey(k => new { k.ItemId, k.TagId });
        }
    }
}
