﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ABDemo.DataAccess.Model;
using Microsoft.EntityFrameworkCore;

namespace ABDemo.DataAccess.Repository
{
    public interface IItemsRepository
    {
        Task<List<Item>> GetItemsList();
        string GetCatgoryNameById(int categoryId);
        Demo1 GetDemo1();
    }
    public class ItemsRepository : IItemsRepository
    {
        private ABDemoContext _db = null;
        public ItemsRepository(ABDemoContext db)
        {
            _db = db;
        }

        public string GetCatgoryNameById(int categoryId)
        {
            return _db.Category.Where(a => a.CategoryId == categoryId).Select(c => c.CategoryName).FirstOrDefault();
        }

        public async Task<List<Item>> GetItemsList()
        {
            return await _db.Item
                //.Include(i => i.Category)
                .Include(i => i.SubCategory)
                .Include(i => i.ItemTag).ThenInclude(t => t.Tag)
                .ToListAsync();
        }
        public Demo1 GetDemo1()
        {
            return new Demo1 { Id = 100 };
        }
    }
    public class Demo1
    {
        public int Id { get; set; }
    }
}
