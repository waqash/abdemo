﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace ABDemo.DataAccess.Model
{
    [Table("Item")]
    public class Item
    {
        [Key]
        public int ItemId { get; set; }
        public int CategoryId { get; set; }
        public int SubCategoryId { get; set; }
        public string ItemName { get; set; }
        public string Descritption { get; set; }
        public virtual Category Category { get; set; }
        public virtual SubCategory SubCategory { get; set; }
        public virtual List<ItemTag> ItemTag { get; set; }
    }
}