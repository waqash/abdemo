﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace ABDemo.DataAccess.Model
{
    [Table("ItemTag")]
    public class ItemTag
    {
        [Key]
        public int ItemId { get; set; }
        public int TagId { get; set; }
        public Tag Tag { get; set; }
    }
}