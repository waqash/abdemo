﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ABDemo.DataAccess;
using ABDemo.DataAccess.Repository;
using ABDemo.Service;
using ABDemo.Service.Sales;
using AutoMapper;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace ABDemo.Web
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<ABDemoContext>(options => options.UseSqlServer(Configuration.GetConnectionString("ABDemoConnection")));

            services.AddAutoMapper(typeof(MappingProfile));

            services.AddScoped<IItemsRepository, ItemsRepository>();
            services.AddScoped<ISalesService, SalesService>();

            services.AddMvc();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseMvc();
        }
    }
}
