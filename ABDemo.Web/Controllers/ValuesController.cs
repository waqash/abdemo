﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ABDemo.Dto;
using ABDemo.Dto.Sales;
using ABDemo.Service.Sales;
using Microsoft.AspNetCore.Mvc;

namespace ABDemo.Web.Controllers
{
    [Route("api/[controller]")]
    public class ValuesController : Controller
    {
        private ISalesService _salesService;
        public ValuesController(ISalesService salesService)
        {
            _salesService = salesService;
        }
        // GET api/values
        [HttpGet]
        public async Task<IActionResult> Get()
        {
            Demo2Dto d2 = _salesService.GetDemo();
            List<SaleItemDto> list = await _salesService.GetItemsList();
            return Ok(list);
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public string Get(int id)
        {
            return "value";
        }

        // POST api/values
        [HttpPost]
        public void Post([FromBody]string value)
        {
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
