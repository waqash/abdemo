﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ABDemo.Dto.Sales
{
    public class SaleItemDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Category { get; set; }
        public string SubCategory { get; set; }
        public string Description { get; set; }
        public List<ItemTagDto> Tags { get; set; }
    }
    public class ItemTagDto
    {
        public int Id { get; set; }
        public string tagName { get; set; }
    }
}
