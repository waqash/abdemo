﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ABDemo.Dto
{
    public class Demo2Dto
    {
        public int ID { get; set; }
        public string DemoName { get; set; }
        public int DemoValue { get; set; }
        public DemoMember member { get; set; }
    }
    public class DemoMember
    {
        public int Id { get; set; }
        public string DemoMeberName { get; set; }
    }
}
